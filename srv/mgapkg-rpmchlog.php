<?php

/*
 * Copyright (C) 2017  Neal Gompa
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, If not, see <http://www.gnu.org/licenses/>.
 */


function is_valid_json($str) {
    json_decode($str);
    return json_last_error() == JSON_ERROR_NONE;
}

$json_str = file_get_contents("php://input");

if (strlen($json_str) > 0 && is_valid_json($json_str)) {
    $json = json_decode($json_str);
}

$svnurl = $json->{"svnrepo"};
$revision = $json->{"svnrev"};
$oldlogs = $json->{"oldlogs"};

if ($oldlogs === true) {
    $repsys_cmd = "repsys -r {$revision} -o {$svnurl}";
} else {
    $repsys_cmd = "repsys -r {$revision} {$svnurl}";
}

$pkg_rpmlog = shell_exec($repsys_cmd);

header('Content-Type: text/plain');
echo "${pkg_rpmlog}\n";
