# Simple mgapkg scripts for Koji to build Mageia packages

This repository contains scripts for enabling Koji to retrieve from Mageia Dist-SVN
and prepare the sources properly to build correct packages.

There are two sets for scripts:

## `bin/mgapkg-koji*`

The `mgapkg-koji` scripts are installed into the Koji builder chroot for retrieving
the sources from the Mageia binrepo and appending the changelog to the spec file
just before building it.

## `srv/mgapkg-rpmchlog.php`

The `mgapkg-rpmchlog.php` script takes JSON data sent by `mgapkg-koji` and returns
a stream of changelog data that can be appended to the spec file. This is used
to allow chroots in a secure Koji environment to access a remote script for get
the changelog data to append when `repsys` isn't installed in the chroot.
